#!/usr/bin/env python
# -*- coding: utf-8 -*-

from funkload.FunkLoadTestCase import FunkLoadTestCase
import socket
import os
import unittest
import logging

#import http_server


SERVER_HOST = os.environ.get('SERVER', '194.29.175.240')
SERVER_PORT = int(os.environ.get('PORT', 6677))
spodziewana_odp=''

class TestServer(FunkLoadTestCase):
    def test_dialog(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((SERVER_HOST, SERVER_PORT))

        sock.sendall('GET / HTTP/1.1')

        answer = ''
        done = False
        buffer = 4096

        while not done:
            part = sock.recv(buffer)

            if len(part) < buffer:
                done = True
            answer += part

        self.assertEqual(answer)
        sock.close()
        #print(sss)



if __name__ == '__main__':
    unittest.main()